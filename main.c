#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define MAX_CONTACTS 4
typedef struct {
  char nombre[20];
  char apellido[20];
  char telefono[12];
  char mail[20];
} contacto;
typedef struct {
  char nombre[20];
  contacto contactos[MAX_CONTACTS];
  int length;
} agenda;
//AGENDA
void cargarContacto(agenda * agenda);
void printContact(contacto contacto);
void iniAgenda(agenda * agenda);
void printAgenda(agenda agenda);
int posContact(agenda agenda, char * str, int type);
void listarContactos(agenda agenda);
void eliminarContacto(agenda * agenda, int pos);
//STRING
char * strToUpper(char * str);
char * strToLower(char * str);
bool matchStr(const char * str1, const char * str2);
//STYLE
void headerApp(char * title,int padding);
void warningMsj(char * msj);
void printPhrase(char * msj, int limit);
//MAIN
int main(int argc, char const *argv[]){
  int opt, p, i;
  char auxStr[20], formColor[5] = "";
  agenda miAgenda;
  iniAgenda(&miAgenda);
  while(opt!=5){
    strcpy(formColor,(miAgenda.length == 4 ? "\033[31m" : ( miAgenda.length >= 2 ? "\033[33m" : "\033[32m")));
    system("cls");
    printf(" \033[1m\033[7m             AGENDA           %s Uso: (%d/%d) \033[0m\n",formColor,miAgenda.length,MAX_CONTACTS);
    printf(" \033[1m1\033[0m. Agendar Contacto\n \033[1m2\033[0m. Buscar Contacto\n \033[1m3\033[0m. Mostrar Todos\n");
    printf(" \033[1m4\033[0m. Eliminar contacto \n \033[1m5\033[0m. Salir\n > ");
    scanf("%d",&opt);
    switch(opt){
      case 1: // Agendar contacto
       system("cls");
       // printf(" ////      NUEVO CONTACTO      ////\n");
       headerApp((char*) "nuevo contacto",42);
       if(miAgenda.length < 4){
         cargarContacto(&miAgenda);
       } else {
         warningMsj((char*) "No hay espacio de almacenamiento borre uno o mas contactos para continuar");
         system("pause");
       }
       break;
      case 2: // Buscar contacto
       system("cls");
       if(miAgenda.length != 0){
         while(opt != 3 && opt < 4){
         system("cls");
         headerApp((char*) "buscar contacto",42);
         printf(" Seleccione criterio de busqueda: \n \033[1m1.\033[0m Buscar por nombre\n");
         printf(" \033[1m2.\033[0m Buscar por apellido \n \033[1m3.\033[0m Volver al menu principal\n > ");
         scanf("%d", &opt);
         if(opt <= 2){
           printf(" Ingrese palabra clave (Sin espacios): ");
           // fgets(auxStr,20,stdin);
           scanf("%s", auxStr);
           p = posContact(miAgenda,auxStr,opt);
           if(p != -1){
             for(i = p; i < miAgenda.length; i++){
               if(matchStr(strToLower(auxStr),strToLower(opt == 1 ? miAgenda.contactos[i].nombre :  miAgenda.contactos[i].apellido))){
                 printContact(miAgenda.contactos[i]);
               }
             }
           } else {
             warningMsj((char*) "No se encontraron coincidencias...");
           }
           system("pause");
         }
       }
       } else {
         headerApp((char*) "Buscar contactos",42);
         warningMsj((char*) "Agenda vacia...");
         system("pause");
       }
       break;
      case 3: // Mostrar todos
       system("cls");
       headerApp((char*) "contactos",42);
       // printf("////       CONTACTOS       ////\n");
       if(miAgenda.length != 0){
         printAgenda(miAgenda);
       } else {
         warningMsj((char*) "No hay contactos que mostrar...");
       }
       system("pause");
       break;
      case 4: // Borrar contacto
       while(opt != 0){
         system("cls");
         headerApp((char*) "borrar contactos",42);
         if(miAgenda.length != 0){
           printf(" Seleccione un contacto, para salir ingrese \"0\": \n");
           listarContactos(miAgenda);
           printf(" > ");
           scanf("%d",&opt);
           if(opt > 0 && (opt - 1) < miAgenda.length){
             eliminarContacto(&miAgenda, opt - 1);
           }
         } else {
           warningMsj((char*) "No hay contactos que eliminar...");
           system("pause"); opt = 0;
         }
       }
       break;
      case 5: break; // salir
      default:
       warningMsj((char*) "Ingrese una opcion valida...");
       system("pause");
       break;
    }
  }
  printf(" Hasta luego...");
  return 0;
}
void printContact(contacto contacto){
  printf(" \033[1m %s, %s\033[0m\n", strToUpper(contacto.apellido), contacto.nombre);
  printf("   \033[4mTelefono:\033[0m %s\n", contacto.telefono);
  printf("   \033[4mMail:\033[0m %s\n", contacto.mail);
}
void printAgenda(agenda agenda){
  int i; for(i = 0; i < agenda.length; i++){printContact(agenda.contactos[i]);}
}
void iniAgenda(agenda * agenda){
  contacto voidContact; char strVoid[] = ""; int i = 0;
  strcpy(voidContact.nombre,strVoid);
  strcpy(voidContact.apellido,strVoid);
  strcpy(voidContact.telefono,strVoid);
  strcpy(voidContact.mail,strVoid);
  while (i < MAX_CONTACTS) {agenda -> contactos[i] = voidContact;i++;}
  agenda -> length = 0;
}
void cargarContacto(agenda * agenda){
  if(agenda -> length < MAX_CONTACTS){
    contacto auxContacto; fflush(stdin);
    printf(" \033[1mNombre\033[0m \n ");
    gets(auxContacto.nombre);
    printf(" \033[1mApellido\033[0m \n ");
    gets(auxContacto.apellido);
    printf(" \033[1mTelefono\033[0m \n ");
    gets(auxContacto.telefono);
    printf(" \033[1mMail\033[0m \n ");
    gets(auxContacto.mail);
    agenda -> contactos[agenda -> length] = auxContacto;
    agenda -> length++;
  }
}
void listarContactos(agenda agenda){
  int i; for(i = 0; i < agenda.length;i++){
    printf(" \033[1m%d)\033[0m %s, %s\n",i+1,agenda.contactos[i].apellido, agenda.contactos[i].nombre);
  }
}
int posContact(agenda agenda, char * str, int type){
  int ret = -1, i; char auxA[20], auxB[20];
  for(i = 0; i < agenda.length; i++){
    strcpy(auxA,strToLower(str));
    strcpy(auxB,strToLower(type == 1 ? agenda.contactos[i].nombre : agenda.contactos[i].apellido));
    if(matchStr(auxA,auxB)){ret = i;break;}
  }
  return ret;
}
void eliminarContacto(agenda * agenda, int pos){
  if(pos >= 0 && pos < agenda -> length){
    int i = pos;
    while(i < agenda -> length){agenda -> contactos[i] = agenda -> contactos[i+1];i++;}
    agenda -> length--;
  }
}
char * strToUpper(char * str){
  int i = 0; static char ret[150];
  while(str[i] != '\0'){
    ret[i] = str[i] >= 97 && str[i] <= 122 ? str[i] - 32 : str[i];
    i++;
  } ret[i] = '\0';
  return ret;
}
char * strToLower(char * str){
  int i = 0;static char ret[150];
  while(str[i] != '\0'){
    ret[i] = str[i] >= 65 && str[i] <= 90 ? str[i] + 32 : str[i];
    i++;
  } ret[i] = '\0';
  return ret;
}
void headerApp(char * title,int padding){
  char space[255];int i = 0;
  while (i< (int) (padding - strlen(title))/2){space[i] = 32;i++;} space[i] = '\0';
  printf(" \033[1m\033[7m%s%s%s\033[0m\n",space,strToUpper(title),space);
}
void warningMsj(char * msj){
  printf("\n \033[33m");
  printf("[!] ");
  printPhrase(msj,37);
  printf("\033[0m\n\n");
}
void printPhrase(char * msj, int limit){
  int i = 0; while (msj[i] != '\0') {
    printf("%c", msj[i]);
    if(i >= limit && i % limit == 0) printf("\n");
    i++;
  }
}
bool matchStr(const char * str1, const char * str2){
  int i = 0, j = 0, aux = 0;
  while(str1[i] != '\0'){
    if(str1[i] == str2[j]){
      aux++; j++;
      if((unsigned int) aux == strlen(str2)){break;}
    } else {aux = 0; j = 0;} i++;
  }
  return ((unsigned int) aux == strlen(str2));
}
